package com.magorasystems.authapi.spring.security;

import com.magorasystems.authapi.token.RequestTokenExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kemenov on 22/09/2017.
 */
public class TokenAuthenticationExtractorImpl implements AuthenticationExtractor {

    private final RequestTokenExtractor requestTokenExtractor;

    public TokenAuthenticationExtractorImpl(RequestTokenExtractor requestTokenExtractor) {
        this.requestTokenExtractor = requestTokenExtractor;
    }

    @Override
    public UsernamePasswordAuthenticationToken extractAuthentication(HttpServletRequest request) {
        String s = requestTokenExtractor.extractToken(request);
        if (s == null || s.isEmpty()) {
            return null;
        }
        return new AuthenticationTokenHolder(s);
    }

    @Override
    public boolean supports(HttpServletRequest request) {
        return requestTokenExtractor.supports(request);
    }

    public static final class AuthenticationTokenHolder extends UsernamePasswordAuthenticationToken {

        private final String token;

        public AuthenticationTokenHolder(String token) {
            super(null, null);
            this.token = token;
        }

        public String getToken() {
            return token;
        }
    }

}
