package com.magorasystems.authapi.spring.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Fyodor Kemenov
 *
 */
public class ProtocolAccessDeniedHandler implements AccessDeniedHandler {

    private final ProtocolSecurityExceptionRenderer protocolErrorResolver;

    public ProtocolAccessDeniedHandler(ProtocolSecurityExceptionRenderer protocolErrorResolver) {
        this.protocolErrorResolver = protocolErrorResolver;
    }

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        this.protocolErrorResolver.render(httpServletRequest, httpServletResponse, e);
    }
}
