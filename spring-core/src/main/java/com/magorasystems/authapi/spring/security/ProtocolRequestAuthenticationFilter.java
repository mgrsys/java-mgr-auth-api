package com.magorasystems.authapi.spring.security;

import com.magorasystems.authapi.token.DefaultRequestTokenExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class ProtocolRequestAuthenticationFilter implements Filter, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(ProtocolRequestAuthenticationFilter.class);
    private AuthenticationEntryPoint authenticationEntryPoint = new ProtocolAuthenticationEntryPoint(
            new ProtocolSecurityExceptionRendererImpl()
    );
    private AuthenticationManager authenticationManager;

    private AuthenticationExtractor authenticationExtractor = new TokenAuthenticationExtractorImpl(
            new DefaultRequestTokenExtractor()
    );

    private AuthenticationEventPublisher eventPublisher = new NullEventPublisher();


    public ProtocolRequestAuthenticationFilter() {
    }

    public void setAuthenticationEntryPoint(AuthenticationEntryPoint authenticationEntryPoint) {
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void setAuthenticationExtractor(AuthenticationExtractor authenticationExtractor) {
        this.authenticationExtractor = authenticationExtractor;
    }

    public void afterPropertiesSet() {
        Assert.state(this.authenticationManager != null, "AuthenticationManager is required");
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if (authenticationExtractor.supports(request)) {

            try {
                Authentication authentication = authenticationExtractor.extractAuthentication(request);
                if (authentication == null) {
                    if (this.isAuthenticated()) {
                        logger.debug("Clearing security context.");
                        SecurityContextHolder.clearContext();
                    }
                    logger.debug("No token in request, will continue chain.");
                } else {
                    Authentication authResult = this.authenticationManager.authenticate(authentication);

                    logger.debug("Authentication success: {}", authResult);

                    this.eventPublisher.publishAuthenticationSuccess(authResult);
                    SecurityContextHolder.getContext().setAuthentication(authResult);
                }
            } catch (AuthenticationException ex) {
                SecurityContextHolder.clearContext();
                logger.debug("Authentication request failed: {}", ex);
                this.eventPublisher.publishAuthenticationFailure(new BadCredentialsException(ex.getMessage(), ex), new PreAuthenticatedAuthenticationToken("access-token", "N/A"));
                this.authenticationEntryPoint.commence(request, response, new InsufficientAuthenticationException(ex.getMessage(), ex));
                return;
            }
            chain.doFilter(request, response);
        } else {
            chain.doFilter(request, response);
        }

    }

    private boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken);
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }

    private static final class NullEventPublisher implements AuthenticationEventPublisher {
        private NullEventPublisher() {
        }

        public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        }

        public void publishAuthenticationSuccess(Authentication authentication) {
        }
    }


}