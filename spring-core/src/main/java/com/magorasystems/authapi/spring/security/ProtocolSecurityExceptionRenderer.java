package com.magorasystems.authapi.spring.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Fyodor Kemenov
 *
 */
public interface ProtocolSecurityExceptionRenderer {

    void render(HttpServletRequest request, HttpServletResponse response, Exception exception);

}
