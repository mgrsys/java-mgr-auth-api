package com.magorasystems.authapi.spring.security;

import com.magorasystems.protocolapi.exception.CodeAwareTrouble;
import com.magorasystems.protocolapi.exception.SecurityAwareTrouble;
import org.springframework.security.core.AuthenticationException;

/**
 * Created by kemenov on 07/07/2017.
 */
public class SpringAuthenticationException extends AuthenticationException implements CodeAwareTrouble {

    private final String code;
    private final String field;

    public SpringAuthenticationException(SecurityAwareTrouble ex) {
        super(ex.getMessage());
        this.code = ex.getCode();
        this.field = ex.getField();
    }

    public SpringAuthenticationException(String field, String code, String msg, Throwable t) {
        super(msg, t);
        this.field = field;
        this.code = code;
    }

    public SpringAuthenticationException(String field, String code, String msg) {
        super(msg);
        this.field = field;
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getField() {
        return field;
    }
}
