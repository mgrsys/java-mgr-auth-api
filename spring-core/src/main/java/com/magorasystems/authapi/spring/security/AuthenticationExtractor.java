package com.magorasystems.authapi.spring.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kemenov on 22/09/2017.
 */
public interface AuthenticationExtractor {

    UsernamePasswordAuthenticationToken extractAuthentication(HttpServletRequest request);

    boolean supports(HttpServletRequest request);

}
