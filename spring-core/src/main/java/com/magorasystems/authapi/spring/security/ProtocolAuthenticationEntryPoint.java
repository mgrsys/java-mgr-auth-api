package com.magorasystems.authapi.spring.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProtocolAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final ProtocolSecurityExceptionRenderer protocolErrorResolver;

    public ProtocolAuthenticationEntryPoint(ProtocolSecurityExceptionRenderer protocolErrorResolver) {
        this.protocolErrorResolver = protocolErrorResolver;
    }

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        this.protocolErrorResolver.render(httpServletRequest, httpServletResponse, e);
    }
}