package com.magorasystems.authapi.spring.security;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magorasystems.authapi.exception.InvalidTokenException;
import com.magorasystems.protocolapi.exception.CodeAwareTrouble;
import com.magorasystems.protocolapi.response.ErrorResponse;
import com.magorasystems.protocolapi.response.ErrorResponses;
import com.magorasystems.protocolapi.response.HttpCodes;
import com.magorasystems.protocolapi.response.ResponseCodes;
import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Fyodor Kemenov
 */
public class ProtocolSecurityExceptionRendererImpl implements ProtocolSecurityExceptionRenderer {

    private static final Logger logger = LoggerFactory.getLogger(ProtocolSecurityExceptionRendererImpl.class);

    private final ObjectMapper objectMapper;
    private final String contentType;

    public ProtocolSecurityExceptionRendererImpl() {
        objectMapper = new ObjectMapper();
        contentType = "application/json";
    }

    public ProtocolSecurityExceptionRendererImpl(ObjectMapper mapper) {
        objectMapper = mapper;
        contentType = "application/json";
    }

    @Override
    public void render(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        ErrorResponse responseObj;
        if (exception.getCause() != null && (exception.getCause() instanceof CodeAwareTrouble)) {
            SpringAuthenticationException sae = (SpringAuthenticationException) exception.getCause();
            responseObj = ErrorResponses.singleError(
                    ResponseCodes.ERROR_COMMON_SECURITY_CODE,
                    sae.getCode(),
                    sae.getMessage(),
                    sae.getField()
            );
        } else {
            responseObj = ErrorResponses.singleError(
                    ResponseCodes.ERROR_COMMON_SECURITY_CODE,
                    AuthResponseCodes.ACCESS_TOKEN_INVALID_ERROR,
                    exception.getMessage(),
                    InvalidTokenException.TOKEN_FIELD
            );
        }


        response.setContentType(contentType);
        response.setStatus(HttpCodes.HTTP_SECURITY_UNAUTHORIZED);

        try {
            writeContent(response.getOutputStream(), responseObj);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    protected void writeContent(OutputStream stream, Object object) throws IOException {
        JsonGenerator generator = this.objectMapper.getFactory().createGenerator(stream, JsonEncoding.UTF8);
        this.objectMapper.writeValue(generator, object);
        generator.flush();
    }

}

