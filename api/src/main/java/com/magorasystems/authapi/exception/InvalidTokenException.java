package com.magorasystems.authapi.exception;


import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class InvalidTokenException extends AuthException {

    public static final String TOKEN_FIELD = "accessToken";

    public InvalidTokenException(String code, String message, String field) {
        super(code, message, field);
    }

    public InvalidTokenException(String code, String message, String field, Throwable throwable) {
        super(code, message, field, throwable);
    }

    public InvalidTokenException() {
        super(AuthResponseCodes.ACCESS_TOKEN_INVALID_ERROR, "Access Code Invalid", TOKEN_FIELD);
    }

    public InvalidTokenException(String message) {
        super(AuthResponseCodes.ACCESS_TOKEN_INVALID_ERROR, message, TOKEN_FIELD);
    }


}
