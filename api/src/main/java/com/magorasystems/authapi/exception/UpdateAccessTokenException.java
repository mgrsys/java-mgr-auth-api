package com.magorasystems.authapi.exception;


import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class UpdateAccessTokenException extends AuthException {

    public static final String REFRESH_TOKEN_FIELD = "refreshToken";

    public UpdateAccessTokenException() {
        super(AuthResponseCodes.REFRESH_TOKEN_INVALID_ERROR, "Invalid refresh token", REFRESH_TOKEN_FIELD);
    }

    public UpdateAccessTokenException(String code, String message) {
        super(code, message, REFRESH_TOKEN_FIELD);
    }

    public UpdateAccessTokenException(String exception) {
        super(AuthResponseCodes.REFRESH_TOKEN_INVALID_ERROR, exception, REFRESH_TOKEN_FIELD);
    }

}
