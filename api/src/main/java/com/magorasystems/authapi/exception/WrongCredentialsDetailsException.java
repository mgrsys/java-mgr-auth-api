package com.magorasystems.authapi.exception;


import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class WrongCredentialsDetailsException extends AuthException {

    public WrongCredentialsDetailsException(String message, Throwable ex) {
        super(AuthResponseCodes.INVALID_AUTH_DATA, message, null, ex);
    }

    public WrongCredentialsDetailsException(String code, String message, Throwable ex) {
        super(code, message, null, ex);
    }

    public WrongCredentialsDetailsException(String message) {
        super(AuthResponseCodes.INVALID_AUTH_DATA, message, null);
    }
}
