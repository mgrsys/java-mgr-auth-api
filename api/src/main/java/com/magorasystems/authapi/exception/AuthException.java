package com.magorasystems.authapi.exception;

import com.magorasystems.protocolapi.exception.NonAuthorizedTrouble;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class AuthException extends Exception implements NonAuthorizedTrouble {

    private final String code;
    private final String message;
    private final String field;

    public AuthException(String code, String message, String field, Throwable cause) {
        super(cause);
        this.code = code;
        this.message = message;
        this.field = field;
    }

    public AuthException(String code, String message, String field) {
        this.code = code;
        this.message = message;
        this.field = field;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getField() {
        return field;
    }
}
