package com.magorasystems.authapi.token;


import com.magorasystems.protocolapi.request.auth.AuthRequestHeaders;
import com.magorasystems.protocolapi.request.auth.AuthRequestParameters;

import javax.servlet.http.HttpServletRequest;

/**
 * Implementation of token extractor. This variant finds the token: 1. in request parameters. 2. in request headers.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class DefaultRequestTokenExtractor implements RequestTokenExtractor {

    /**
     * Gets request from request.  1. in request parameters. 2. if empty then in request headers.
     */
    @Override
    public String extractToken(HttpServletRequest request) {

        String header = request.getParameter(AuthRequestParameters.ACCESS_TOKEN_PARAMETER);

        if (header == null) {
            header = request.getHeader(AuthRequestHeaders.ACCESS_TOKEN_HEADER);
        }

        return header;
    }

    @Override
    public boolean supports(HttpServletRequest request) {
        String header = request.getHeader(AuthRequestParameters.ACCESS_TOKEN_PARAMETER);
        return header != null;
    }

}
