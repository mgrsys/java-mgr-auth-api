package com.magorasystems.authapi.token;

import javax.servlet.http.HttpServletRequest;

/**
 * There are the different strategies to extracting token. This interface declares method to do it.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public interface RequestTokenExtractor {


    /**
     * Gets token from request. Returns null in case of the token is not found,
     */
    String extractToken(HttpServletRequest request);


    boolean supports(HttpServletRequest request);


}
