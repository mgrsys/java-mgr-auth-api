package com.magorasystems.authapi.token;


import com.magorasystems.authapi.exception.UpdateAccessTokenException;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface TokenCreator {

    Token createToken(UserInfo info);

    Token refreshToken(String refreshToken) throws UpdateAccessTokenException;

}
