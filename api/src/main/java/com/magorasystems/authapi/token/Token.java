package com.magorasystems.authapi.token;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class Token {

    /* Access Token */
    private final String accessToken;

    /* Access Token Expire */
    private final long accessTokenExpire;

    /* Refresh Token */
    private final String refreshToken;


    public Token(String accessToken, long accessTokenExpire, String refreshToken) {
        this.accessToken = accessToken;
        this.accessTokenExpire = accessTokenExpire;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public long getAccessTokenExpire() {
        return accessTokenExpire;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
