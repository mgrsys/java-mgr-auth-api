package com.magorasystems.authapi.token;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Data
@Builder
public class UserInfo {

    private final String userId;
    private final Collection<String> grants;
    private final String secret;
    private final Map<String, String> data;
    private final String platform;
    private final Date expirationDate;


}
