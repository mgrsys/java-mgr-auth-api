package com.magorasystems.authapi.spring.security.jwt;

import com.magorasystems.authapi.exception.UpdateAccessTokenException;
import com.magorasystems.authapi.token.Token;
import com.magorasystems.authapi.token.TokenCreator;
import com.magorasystems.authapi.token.UserInfo;
import com.magorasystems.protocolapi.request.auth.Platform;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import static com.magorasystems.protocolapi.response.auth.AuthResponseCodes.REFRESH_TOKEN_EXPIRED_ERROR;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (masecugora-systems.com). 2016.
 */
@Getter
@Setter
public class DefaultJwtTokenCreator implements TokenCreator {

    private static final String CUSTOM_FIELD_PREFIX = JWTTokenFields.CUSTOM_PREFIX;

    private final String secret;

    private int mobileTokenLifeMin = 720; //min

    private int webTokenLifeTime = 720; //min

    private int refreshTokenLifeTime = 720 * 10; //min

    private boolean alwaysRefresh = false;

    private int version = 1;

    public DefaultJwtTokenCreator(String secret) {
        this.secret = secret;
    }


    @Override
    public Token createToken(UserInfo userInfo) {
        return createAccessToken(userInfo,
                alwaysRefresh || !Platform.isWeb(userInfo.getPlatform())
        );
    }

    @Override
    public Token refreshToken(String token) throws UpdateAccessTokenException {
        try {
            Claims body = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
            if (body.containsKey(JWTTokenFields.REFRESH) && String.valueOf(true).equals(body.get(JWTTokenFields.REFRESH))) {
                Integer curVersion = body.get(JWTTokenFields.VERSION, Integer.class);
                if (curVersion == null || version != curVersion) {
                    throw new UpdateAccessTokenException("Unsupported version of token");
                }
                String checkToken = body.get(JWTTokenFields.CHECK_CODE, String.class);
                Map<String, String> params = body.keySet().stream()
                        .filter(key -> key.startsWith(CUSTOM_FIELD_PREFIX))
                        .collect(Collectors.toMap(key -> key.substring(JWTTokenFields.CUSTOM_PREFIX.length()), key -> body.get(key, String.class)));
                return this.createAccessToken(
                        UserInfo.builder()
                                .userId(body.getSubject())
                                .secret(checkToken)
                                .platform(body.get(JWTTokenFields.PLATFORM, String.class))
                                .data(params)
                                .build(),
                        true
                );
            } else {
                throw new UpdateAccessTokenException("Probably access token is used instead refresh");
            }
        } catch (UpdateAccessTokenException ex) {
            throw ex;
        } catch (ExpiredJwtException e) {
            throw new UpdateAccessTokenException(REFRESH_TOKEN_EXPIRED_ERROR, e.getMessage());
        } catch (Exception ex) {
            throw new UpdateAccessTokenException();
        }
    }


    private String createToken(UserInfo userInfo, Date time, boolean refresh) {
        Claims claims = Jwts.claims().setExpiration(time).setSubject(userInfo.getUserId());
        claims.put(JWTTokenFields.REFRESH, String.valueOf(refresh));
        claims.put(JWTTokenFields.CHECK_CODE, userInfo.getSecret());
        claims.put(JWTTokenFields.VERSION, version);
        claims.put(JWTTokenFields.PLATFORM, userInfo.getPlatform());

        if (userInfo.getData() != null) {
            userInfo.getData().forEach(
                    (key, value) -> claims.put(CUSTOM_FIELD_PREFIX + key, value)
            );
        }
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, this.secret).compact();
    }

    private Token createAccessToken(UserInfo userKey, boolean needRefresh) {
        // if web client that we don't need refresh token and lifetime of access token is looong, else we need it.
        if (!needRefresh) {
            Date exp = DateUtils.addMinutes(new Date(), this.webTokenLifeTime);
            return new Token(this.createToken(userKey, exp, false), exp.getTime(), null);
        } else {
            Date exp = DateUtils.addMinutes(new Date(), this.mobileTokenLifeMin);
            return new Token(
                    this.createToken(userKey, exp, false),
                    exp.getTime(),
                    this.createToken(userKey, DateUtils.addMinutes(new Date(), this.refreshTokenLifeTime), true)
            );
        }

    }

}