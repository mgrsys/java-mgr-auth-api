package com.magorasystems.authapi.spring.security.jwt;

/**
 * Created by kemenov on 23.12.2016.
 * <p>
 * Constant file includes keys of JWT token
 */
public final class JWTTokenFields {

    public static final String REFRESH = "refresh";
    public static final String CHECK_CODE = "cc";
    public static final String CUSTOM_PREFIX = "cf.";
    public static final String VERSION = "vrs";
    public static final String PLATFORM = "pltf";

    private JWTTokenFields() {
    }

}
