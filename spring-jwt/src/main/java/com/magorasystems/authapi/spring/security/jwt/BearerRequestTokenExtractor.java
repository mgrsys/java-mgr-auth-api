package com.magorasystems.authapi.spring.security.jwt;


import com.magorasystems.authapi.token.RequestTokenExtractor;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class BearerRequestTokenExtractor implements RequestTokenExtractor {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String BEARER_PREFIX = "Bearer ";

    @Override
    public String extractToken(HttpServletRequest request) {
        String header = request.getHeader(AUTHORIZATION_HEADER_NAME);

        if (header == null || !header.startsWith(BEARER_PREFIX)) {
            return null;
        }

        return header.substring(BEARER_PREFIX.length());
    }

    @Override
    public boolean supports(HttpServletRequest request) {
        String header = request.getHeader(AUTHORIZATION_HEADER_NAME);
        return header != null && header.startsWith(BEARER_PREFIX);
    }

}
