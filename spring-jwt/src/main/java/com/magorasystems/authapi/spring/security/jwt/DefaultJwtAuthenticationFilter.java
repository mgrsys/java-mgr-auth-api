package com.magorasystems.authapi.spring.security.jwt;

import com.magorasystems.authapi.spring.security.ProtocolRequestAuthenticationFilter;
import com.magorasystems.authapi.spring.security.TokenAuthenticationExtractorImpl;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class DefaultJwtAuthenticationFilter extends ProtocolRequestAuthenticationFilter {

    public DefaultJwtAuthenticationFilter() {
        setAuthenticationExtractor(
                new TokenAuthenticationExtractorImpl(
                        new BearerRequestTokenExtractor()
                )
        );
    }
}