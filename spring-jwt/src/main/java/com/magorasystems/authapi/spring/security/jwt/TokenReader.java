package com.magorasystems.authapi.spring.security.jwt;


import com.magorasystems.authapi.exception.InvalidTokenException;
import com.magorasystems.authapi.token.UserInfo;

import java.util.function.Function;

/**
 * Created by kemenov on 09.02.2017.
 */
public interface TokenReader {

    UserInfo readToken(String token, String checkCode) throws InvalidTokenException;

    UserInfo readTokenGet(String token, Function<String, String> getCheckCode) throws InvalidTokenException;

}
