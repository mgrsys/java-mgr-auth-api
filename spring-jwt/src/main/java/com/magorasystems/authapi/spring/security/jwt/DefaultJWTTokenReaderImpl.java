package com.magorasystems.authapi.spring.security.jwt;

import com.magorasystems.authapi.exception.InvalidTokenException;
import com.magorasystems.authapi.token.UserInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.magorasystems.protocolapi.response.auth.AuthResponseCodes.*;


/**
 * Created by kemenov on 09.02.2017.
 */
@Getter
@Setter
public class DefaultJWTTokenReaderImpl implements TokenReader {

    private static final String TOKEN_FIELD = InvalidTokenException.TOKEN_FIELD;
    private final String secret;
    private int version = 1;

    public DefaultJWTTokenReaderImpl(String secret) {
        this.secret = secret;
    }

    @Override
    public UserInfo readToken(final String token, String checkCode) throws InvalidTokenException {

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            Map<String, String> params = body.keySet().stream()
                    .filter(key -> key.startsWith(JWTTokenFields.CUSTOM_PREFIX))
                    .collect(Collectors.toMap(key -> key.substring(JWTTokenFields.CUSTOM_PREFIX.length()), key -> body.get(key, String.class)));

            Integer curVersion = body.get(JWTTokenFields.VERSION, Integer.class);
            if (curVersion == null || version != curVersion) {
                throw new InvalidTokenException("Unsupported version of token");
            }

            String cc = body.get(JWTTokenFields.CHECK_CODE, String.class);


            if (cc == null || cc.isEmpty()) {
                throw new InvalidTokenException(INVALID_CHECK_CODE, "Check code isn't valid", TOKEN_FIELD);
            } else {
                if (!cc.equals(checkCode)) {
                    throw new InvalidTokenException(INVALID_CHECK_CODE, "Check code isn't valid", TOKEN_FIELD);


                }
            }

            if (!body.get(JWTTokenFields.REFRESH, String.class).equals("false")) {
                throw new InvalidTokenException(INVALID_CHECK_CODE, "Refresh token is used instead access", TOKEN_FIELD);
            }

            return UserInfo.builder()
                    .userId(body.getSubject())
                    .secret(cc)
                    .platform(body.get(JWTTokenFields.PLATFORM, String.class))
                    .expirationDate(body.getExpiration())
                    .data(params)
                    .build();

        } catch (InvalidTokenException e) {
            throw e;
        } catch (ExpiredJwtException e) {
            throw new InvalidTokenException(ACCESS_TOKEN_EXPIRED_ERROR, e.getMessage(), TOKEN_FIELD, e);
        } catch (Exception e) {
            throw new InvalidTokenException(ACCESS_TOKEN_INVALID_ERROR, "Access token invalid", TOKEN_FIELD, e);
        }

    }

    @Override
    public UserInfo readTokenGet(String token, Function<String, String> getCheckCode) throws InvalidTokenException {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            String apply = getCheckCode.apply(body.getSubject());
            return readToken(token, apply);

        } catch (ExpiredJwtException e) {
            throw new InvalidTokenException(ACCESS_TOKEN_EXPIRED_ERROR, e.getMessage(), TOKEN_FIELD, e);
        } catch (InvalidTokenException e) {
            throw e;
        } catch (Exception e) {
            throw new InvalidTokenException(ACCESS_TOKEN_INVALID_ERROR, "Access token invalid", TOKEN_FIELD, e);
        }
    }

}
